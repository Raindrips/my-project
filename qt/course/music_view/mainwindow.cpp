#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QFileDialog>
#include <QTime>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    player=new QMediaPlayer(this);
    playlist=new QMediaPlaylist(this);

    //设置播放模式
    ui->comboBox->addItem("只播放一次");
    ui->comboBox->addItem("单曲循环");
    ui->comboBox->addItem("顺序");
    ui->comboBox->addItem("循环");
    ui->comboBox->addItem("随机");

    //设置播放列表
    player->setPlaylist(playlist);

    //声音默认是50
    player->setVolume(50);

    //设置播放模式  循环播放
    playlist->setPlaybackMode(QMediaPlaylist::PlaybackMode::Loop);

    //播放状态
    connect(player,&QMediaPlayer::stateChanged,this,&MainWindow::on_stateChanged);

    //播放进度
    connect(player,&QMediaPlayer::positionChanged,this,&MainWindow::on_positionChanged);

    // 曲目修改
    connect(player,&QMediaPlayer::durationChanged,this,&MainWindow::MainWindow::on_durationChanged);

    // 播放列表修改
    connect(playlist,&QMediaPlaylist::currentIndexChanged,this,&MainWindow::on_currentIndexChanged);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile()
{
    QString path=QDir::homePath();
    QString title="音频文件";
    QString filter="音频文件(*.mp3 *.wav *.ogg);;所有文件(*.*)";

    QStringList fileNameList=QFileDialog::getOpenFileNames(this,title,path,filter);
    if(fileNameList.isEmpty()){
        return;
    }
    for(auto filename:fileNameList){
        //添加到音乐播放列表
        playlist->addMedia(QUrl::fromLocalFile(filename));

        //列表控件
        QFileInfo fileInfo(filename);
        ui->listWidget->addItem(fileInfo.fileName());
    }

}

void MainWindow::on_stateChanged(QMediaPlayer::State state)
{
    bool isPlaying=state==QMediaPlayer::State::PlayingState;

    ui->pushButton_play->setEnabled(!isPlaying);
    ui->pushButton_pause->setEnabled(isPlaying);
    ui->pushButton_stop->setEnabled(isPlaying);
}

void MainWindow::on_positionChanged(qint64 position)
{
    if(ui->horizontalSlider_poition->isSliderDown()){
        return;
    }
    ui->horizontalSlider_poition->setSliderPosition(position);

    //格式时间
    QTime time(0,0,0);
    time=time.addMSecs(position);
    QString timeString=time.toString("hh:mm:ss");
    ui->label_position->setText(timeString);
}

void MainWindow::on_durationChanged(qint64 duration)
{
    ui->horizontalSlider_poition->setMaximum(duration);

    //格式时间
    QTime time(0,0,0);
    time=time.addMSecs(duration);
    QString timeString=time.toString("hh:mm:ss");
    ui->label_duration->setText(timeString);
}

void MainWindow::on_currentIndexChanged(int index)
{
    ui->listWidget->setCurrentRow(index);
}


void MainWindow::on_pushButton_add_clicked()
{
    //添加文件
    openFile();
    //如果正在播放,将要播放的元素反在首位
    if(player->state()!=QMediaPlayer::PlayingState){
        playlist->setCurrentIndex(0);
    }
    player->play();


}

void MainWindow::on_pushButton_remove_clicked()
{
    //移除列表
    int pos=ui->listWidget->currentRow();

    //从当前播放列表进行移除,要进行判断是否是当前正在播放的曲目
    if(playlist->currentIndex()==pos){
        int next=pos+1;
        if(playlist->mediaCount()>0){
            playlist->setCurrentIndex(next);
        }
        else{
           ui->label_music->setText("无曲目");
           return;
        }
    }

    //从列表中移除
    playlist->removeMedia(pos);
    QListWidgetItem* item=ui->listWidget->takeItem(pos);
    ui->listWidget->removeItemWidget(item);
}

void MainWindow::on_pushButton_exit_clicked()
{
    exit(0);
}

void MainWindow::on_pushButton_play_clicked()
{
    if(playlist->currentIndex()<0){
        return;
    }
    player->play();
}

void MainWindow::on_pushButton_pause_clicked()
{
    player->pause();
}

void MainWindow::on_pushButton_stop_clicked()
{
    player->stop();
}

void MainWindow::on_pushButton_mutel_clicked()
{
    bool isMute=player->isMuted();
    player->setMuted(!isMute);
}

void MainWindow::on_horizontalSlider_volumn_valueChanged(int value)
{
    player->setVolume(value);
}

void MainWindow::on_horizontalSlider_poition_sliderMoved(int position)
{
     player->setPosition(position);
}

void MainWindow::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    int index= ui->listWidget->currentIndex().row();
    qDebug()<<index;
    playlist->setCurrentIndex(index);
    player->play();
    ui->label_music->setText(item->text());
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    playlist->setPlaybackMode((QMediaPlaylist::PlaybackMode)index);
}
