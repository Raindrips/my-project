#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QListWidget>
#include <QMainWindow>
#include <QMediaPlayer>
#include <QMediaPlaylist>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void openFile();

private slots:

    void on_stateChanged(QMediaPlayer::State state);

    void on_positionChanged(qint64 position);

    void on_durationChanged(qint64 duration);

    void on_currentIndexChanged(int index);

    void on_pushButton_add_clicked();

    void on_pushButton_remove_clicked();

    void on_pushButton_exit_clicked();

    void on_pushButton_play_clicked();

    void on_pushButton_pause_clicked();

    void on_pushButton_stop_clicked();

    void on_pushButton_mutel_clicked();

    void on_horizontalSlider_volumn_valueChanged(int value);

    void on_horizontalSlider_poition_sliderMoved(int position);

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::MainWindow *ui;

    QMediaPlayer *player;           //播放器
    QMediaPlaylist* playlist;       //播放列表


};
#endif // MAINWINDOW_H
