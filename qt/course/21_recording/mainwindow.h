#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAudioRecorder>
#include <QAudioProbe>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    //初始化UI控件
    void initUI();
    void initAudio();


    QAudioProbe*probe;          //声音探测器
    QAudioRecorder* recoder;    //音频录音

    void openSaveFile();

public slots:
    void on_stateChanged(QMediaRecorder::State state);

    void on_durationChanged(qint64 duration);

    void on_audioBufferProbed(const QAudioBuffer& buffer);
private slots:
    void on_actionRecord_triggered();
    void on_pushButton_file_clicked();
    void on_actionStop_triggered();
    void on_actionPause_triggered();
    void on_actionExit_triggered();
};
#endif // MAINWINDOW_H
