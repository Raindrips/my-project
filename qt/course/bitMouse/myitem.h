#ifndef MYITEM_H
#define MYITEM_H

#include <QCursor>
#include <QGraphicsPixmapItem>

enum class State{
    empty=0,                //空地
    show_hamster,           //地鼠
    hit_hamster             //地鼠被击打
};

class MyItem:public QGraphicsPixmapItem
{
public:
    MyItem();

    //设置开始
    void setStart(bool start);
    //判断是否开始
    bool isStart();

    //设置地鼠
    void setHamster(bool hamster);
    // 判断是否是地鼠
    bool isHamster();

    //修改状态
    void setState(State state);
    State getState();

    //仿函数       功能同函数指针
    std::function<void()> beatHamster;


protected:
    //注册鼠标按下事件
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

    //注册鼠标松开事件
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    bool _isStart;
    bool _isHamster;
    State _state;

    QCursor up;     //光标抬起的状态
    QCursor down;   // 光标落下的状态
};

#endif // MYITEM_H
