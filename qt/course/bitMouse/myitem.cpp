#include "myitem.h"

#include <QGraphicsSceneMouseEvent>

MyItem::MyItem()
{
    _isStart=false;
    this->up=QCursor(QPixmap(":/pic/up.png"));
    this->down=QCursor(QPixmap(":/pic/down.png"));

    setState(State::empty);
}

void MyItem::setStart(bool start)
{
    _isStart=start;
    setCursor(up);
}

bool MyItem::isStart()
{
    return  _isStart;
}

void MyItem::setHamster(bool hamster)
{
    _isHamster=hamster;
}

bool MyItem::isHamster()
{
    return  _isHamster;
}

void MyItem::setState(State state)
{
    _state=state;
    switch (state) {

    case State::empty:
        this->setPixmap(QPixmap(":/pic/map.jpg"));         //设置当前显示图片为空地
        _isHamster= false;
        break;

    case State::show_hamster:
        this->setPixmap(QPixmap(":/pic/mouse.jpg"));         //设置当前显示图片为地图
       _isHamster= true;
        break;

    case State::hit_hamster:
        this->setPixmap(QPixmap(":/pic/hit_mouse.jpg"));         //设置当前显示图片为地鼠被打
        _isHamster= false;
        break;
    }
}

State MyItem::getState()
{
    return _state;
}

void MyItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(!_isStart){
        return;
    }

    //鼠标左键被点击
    if(event->button()==Qt::MouseButton::LeftButton){
        setCursor(down);
        if(_isHamster){
            if(bool(beatHamster)){
                beatHamster();
            }
            setState(State::hit_hamster);
        }
    }
}

void MyItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    //左键松开
    if(event->button()==Qt::MouseButton::LeftButton){
        setCursor(up);
    }

}
