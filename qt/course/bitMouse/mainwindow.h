#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "game_scene.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();



private slots:
    void on_actionstart_triggered();

    void updateScore();

    void on_actionpause_triggered();

    void on_actionstop_triggered();

private:
    Ui::MainWindow *ui;

    GameScene* gameScene;

    int socre;
};
#endif // MAINWINDOW_H
