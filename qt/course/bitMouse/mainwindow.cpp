#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    gameScene=new GameScene(this);
    ui->graphicsView->setScene(gameScene);
    socre=0;
    connect(gameScene,&GameScene::beatHamster,this,&MainWindow::updateScore);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionstart_triggered()
{
    gameScene->startGame();
    ui->lcdNumber->display(0);
}

void MainWindow::updateScore()
{
    socre++;
    ui->lcdNumber->display(socre);
}

void MainWindow::on_actionpause_triggered()
{
    gameScene->pauseGame();
}

void MainWindow::on_actionstop_triggered()
{
    gameScene->stopGame();
    socre=0;
}
