#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <QGraphicsScene>
#include "myitem.h"

class GameScene : public QGraphicsScene
{
    Q_OBJECT
public:
    GameScene(QObject* parent=nullptr);

    ~GameScene();

public slots:
    void showHamster();

    void startGame();

    void pauseGame();

    void stopGame();

signals:
    void beatHamster();

private:
    QTimer *timer;


    std::vector<MyItem*>itemList;           //所有地鼠类
    const size_t COUNT=16;                  //地鼠坑数量
};

#endif // GAMESCENE_H
