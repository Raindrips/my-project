#include "videowidget.h"

#include <QKeyEvent>

VideoWidget::VideoWidget(QWidget *parent)
{

}

void VideoWidget::setMediaPlayer(QMediaPlayer *player)
{
    this->player=player;
}

void VideoWidget::keyPressEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Escape&&this->isFullScreen()){
        this->setFullScreen(false);
    }
}
