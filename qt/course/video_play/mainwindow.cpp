#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QFileDialog>
#include <QDebug>
#include <QUrl>
#include <QTime>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    player=new QMediaPlayer(this);
    player->setNotifyInterval(1000);

    player->setVideoOutput(ui->video_widget);
    ui->video_widget->setMediaPlayer(player);

    //状态改变事件
    connect(player,&QMediaPlayer::stateChanged,this,&MainWindow::on_stateChanged);

    //播放坐标位置事件
    connect(player,&QMediaPlayer::positionChanged,this,&MainWindow::on_positionChanged);

    //属性改变事件
    connect(player,&QMediaPlayer::durationChanged,this,&MainWindow::on_durationChanged);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_open_clicked()
{
    //使用文件对话框打开文件
    QString path=QDir::homePath();
    QString title="视频文件";
    QString filter="mp4文件(*.mp4);;wmv文件(*.wmv);;所以文件(*.*)";
    QString file=QFileDialog::getOpenFileName(this,title,path);
    qDebug()<<"打开的文件"<< file;
    if(file.isEmpty()){
        return;
    }

    QFileInfo fileInfo(file);
    this->ui->label_name->setText(fileInfo.fileName());

    //TODO 播放
    player->setMedia(QUrl::fromLocalFile(file));
    player->play();

}

void MainWindow::on_stateChanged(QMediaPlayer::State state)
{
    using State=QMediaPlayer::State;
    ui->pushButton_player->setEnabled(!(State::PlayingState==state));
    ui->pushButton_stop->setEnabled(State::PlayingState==state);
    ui->pushButton_volumn->setEnabled(State::PlayingState==state);

}

void MainWindow::on_positionChanged(qint64 position)
{
    if(ui->slider_position->isSliderDown()){
        return;
    }
    ui->slider_position->setSliderPosition(position);
    //使用qt类进行转换
    QTime qtime(0,0,0);
    qtime= qtime.addMSecs(position);
    QString timeString=qtime.toString("hh:mm:ss");
    ui->label_time->setText(timeString);
}

void MainWindow::on_durationChanged(qint64 duration)
{
    ui->slider_position->setMaximum(duration);
    //自己计算转换
    qint64 secs=duration/1000;
    qint64 mins=secs/60;            //分
    qint64 hour=mins/60;            //时
    secs%=60;
    QString timeString=QString("%1:%2:%3").arg(hour).arg(mins).arg(secs);
    ui->label_duration->setText(timeString);
}

void MainWindow::on_pushButton_player_clicked()
{
     player->play();
}

void MainWindow::on_pushButton_stop_clicked()
{
    player->pause();
}

void MainWindow::on_pushButton_volumn_clicked()
{
    //是否是静音状态
    bool isMute=player->isMuted();
    player->setMuted(!isMute);
}

void MainWindow::on_slider_volumn_sliderMoved(int position)
{
    //设置音量
    player->setVolume(position);
}

void MainWindow::on_slider_position_sliderMoved(int position)
{
    //设置视频时长
    player->setPosition(position);
}

void MainWindow::on_pushButton_all_clicked()
{
    ui->video_widget->setFullScreen(true);
}
