#ifndef VIDEOWIDGET_H
#define VIDEOWIDGET_H

#include <QAbstractItemModel>
#include <QVideoWidget>
#include <QMediaPlayer>

class VideoWidget : public QVideoWidget
{
    Q_OBJECT

public:
    VideoWidget(QWidget *parent=nullptr);

    void setMediaPlayer(QMediaPlayer* player);

    void keyPressEvent(QKeyEvent* event) override;

protected:
    QMediaPlayer *player;
};

#endif // VIDEOWIDGET_H
