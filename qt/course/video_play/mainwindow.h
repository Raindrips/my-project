#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_open_clicked();

    void on_stateChanged(QMediaPlayer::State state);

    void on_positionChanged(qint64 position);

    void on_durationChanged(qint64 duration);

    void on_pushButton_player_clicked();

    void on_pushButton_stop_clicked();

    void on_pushButton_volumn_clicked();

    void on_slider_volumn_sliderMoved(int position);

    void on_slider_position_sliderMoved(int position);

    void on_pushButton_all_clicked();

private:
    Ui::MainWindow *ui;

    QMediaPlayer *player;
};
#endif // MAINWINDOW_H
