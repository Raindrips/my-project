#include "widget.h"
#include <QString>
#include <QIcon>
#include <QMenu>
#include <QApplication>
#include <QPixmap>
#include <QDebug>
#include <QCommandLinkButton>
#include <QRadioButton>
#include <QButtonGroup>

Widget::Widget(QWidget *parent) :
    QWidget(parent)

{
    setMinimumSize(500,300);
    setMaximumSize(500,300);
//usePushBtn();
   // useLabel();
    //useCheckBtn();
    useCombobox();
}

Widget::~Widget()
{

}

void Widget::usePushBtn()
{
    pOkBtn = new QPushButton(QString("登陆"),this);
    //设在按钮位置
    pOkBtn->setGeometry(20,20,120,120);
    //设在按钮文字
    pOkBtn->setText(QString("login"));
    //设在字体
    pOkBtn->setFont(QFont("宋体",20));
    //贴图
    QIcon ico(QString("login.png"));
    pOkBtn->setIcon(ico);
    //设置图片大小
    pOkBtn->setIconSize(QSize(50,50));

//再做一个按钮
    pCancelBtn = new QPushButton(QString("退出"),this);
    //设在按钮位置
    pCancelBtn->setGeometry(20,150,80,50);
    //弹出菜单
    QMenu* m1 = new QMenu("xxxooo");
    m1->addAction("文件");
    m1->addAction("目录");
    m1->addAction("装C");
    pCancelBtn->setMenu(m1);

    connect(pOkBtn,SIGNAL(clicked(bool)),this,SLOT(OnlogInBtn()));

}
void Widget::OnlogInBtn()
{
    pCancelBtn->setGeometry(120,150,80,50);
}

void Widget::useLabel()
{
    PUserName = new QLabel("不要翻我牌",this);
    PUserName->setGeometry(20,50,80,50);
    //设在字体
    PUserName->setFont(QFont("宋体",20));
    //显示数组(不多)
     PUserName->setNum(234.123);
    //贴图
    QPixmap pix(QString("xxxooo.png")) ;
    PUserName->setPixmap(pix);
    //设置图片大小
  //  PUserName->setFixedSize(50,50);
    //设在label为图片大小
    //PUserName->resize(QSize(pix.width(),pix.height()));
    //设在图片大小自适应label
    PUserName->setScaledContents(true);

    //继续创建label
    pPassWord =  new QLabel("来咯,进来咯",this);
    pPassWord->setGeometry(120,50,80,50);
    pPassWord->setText(QString("<a href = 'http://www.tanzhouedu.com/'>潭州官网</a>"));
    //打开连接
    pPassWord->setOpenExternalLinks(true);

}

void Widget::useCheckBtn()
{
    pLoginRmb = new QCheckBox(QString("记住密码"),this);
    pLoginRmb->setGeometry(20,50,80,50);
    //提示信息
    pLoginRmb->setToolTip(QString("点击之后密码就保存了"));
    //设在三种状态
    pLoginRmb->setTristate(true);
    //设在选择状态
    pLoginRmb->setCheckState(Qt::Checked);
    //信号槽处理
    connect(pLoginRmb,SIGNAL(stateChanged(int)),this,SLOT(OnChange(int)));

    //链接按钮--单选框
    QCommandLinkButton *plinkBtn = new QCommandLinkButton("下一章节",this);
    plinkBtn->setGeometry(120,50,120,40);

    //单选框
    QRadioButton * pradio1 = new QRadioButton("凤姐",this);
     pradio1->setGeometry(120,100,120,20);
    QRadioButton * pradio2 = new QRadioButton("芙蓉姐姐",this);
     pradio2->setGeometry(120,130,120,20);
    QRadioButton * pradio3 = new QRadioButton("翠花",this);
     pradio3->setGeometry(120,160,120,20);
    QRadioButton * pradio4 = new QRadioButton("老王",this);
     pradio4->setGeometry(120,190,120,20);
    QRadioButton * pradio5 = new QRadioButton("二狗子",this);
    pradio5->setGeometry(120,220,120,20);
    QButtonGroup* xpp = new QButtonGroup(this);
    xpp->addButton(pradio1);
    xpp->addButton(pradio2);
    xpp->addButton(pradio3);

    QButtonGroup* spp = new QButtonGroup(this);
    spp->addButton(pradio4);
    spp->addButton(pradio5);

}

void Widget::OnChange(int sta)
{
    if(sta == Qt::Checked)
    {
        qDebug()<< "现在是勾选";
    }else if(sta == Qt::Unchecked )
    {
          qDebug()<< "现在是 非 勾选";
    }else
    {
         qDebug()<< "现在是 填充 状态";
    }
}

void Widget::useCombobox()
{
    pNameBox = new QComboBox(this);
    //添加字符串
    pNameBox->addItem("PPX_AND_XXP");
    pNameBox->addItem("BY_AND_YB");
    pNameBox->addItem("ZK_AND_FJ");
    pNameBox->addItem("DD_AND_BB");
    //插入数据
    pNameBox->insertItem(2,"danny");
     pNameBox->setGeometry(120,50,130,30);
     //设在选择项
     pNameBox->setCurrentIndex(2);
     //得到当前选择内容
     qDebug()<< pNameBox->currentText();
     qDebug()<< pNameBox->count();
     //设在最多显示数目
     pNameBox->setMaxVisibleItems(3);
    // 槽函数处理(当选择某一个账号后,判断这个人是不是属于潭州人);
}
