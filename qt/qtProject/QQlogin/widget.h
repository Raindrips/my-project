#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>
#include <QComboBox>

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    QPushButton*  pOkBtn;
    QPushButton*  pCancelBtn;
    QLabel *PUserName;
     QLabel *pPassWord;
QCheckBox* pLoginRmb ;
    QComboBox* pNameBox;

private:
    void useLabel();
    void useCheckBtn();
    void usePushBtn();
    void useCombobox();
public slots:
    void OnlogInBtn();
    void OnChange(int); //点击复选框 处理槽函数

};

#endif // WIDGET_H
