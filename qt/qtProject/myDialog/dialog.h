#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QFrame>



class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = 0);
    ~Dialog();
//文件对话框
   QPushButton *fileBtn;
   QLineEdit * fileLineEdit;

   //标准颜色对话框
   QPushButton * colorBtn;
   QFrame * colorFrame;

//标准字体对话框
   QPushButton *FontBtn;
   QLineEdit * FontLineEdit;

//输入对话框
  QPushButton *inputBtn;
   //消息对话框
  QPushButton *MsgBtn;
   QGridLayout * layout;
public slots:
   void FileDlg();
   void ColorDlg();
   void FontDlg();
   void InputDlg();
   void MsgDlg();
};

#endif // DIALOG_H
