#include "dialog.h"
#include <QFileDialog>
#include <QColorDialog>
#include <QFontDialog>
#include <QInputDialog>
#include <QStringList>
#include <QMessageBox>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
   fileBtn = new QPushButton(this);
   fileBtn->setText(QString("标准文件对话框"));
   fileLineEdit = new QLineEdit(this);


    layout  = new QGridLayout(this);
    layout->addWidget(fileBtn,0,0);
    layout->addWidget(fileLineEdit,0,1);
    connect(fileBtn,SIGNAL(clicked(bool)),
            this,SLOT(FileDlg()));

    //标准颜色对话框
    colorBtn = new QPushButton(this);
    colorBtn->setText(QString("标准颜色对话框"));
    colorFrame = new QFrame(this);
    colorFrame->setFrameShape(QFrame::Box);
    colorFrame->setAutoFillBackground(true);
    layout->addWidget(colorBtn,1,0);
    layout->addWidget(colorFrame,1,1);
    connect(colorBtn,SIGNAL(clicked(bool)),
            this,SLOT(ColorDlg()));

//标准字体对话框
    FontBtn = new QPushButton(this);
    FontBtn->setText(QString("标准字体对话框"));
    FontLineEdit = new QLineEdit(this);
    FontLineEdit->setText("DANNY帅帅帅");
    layout->addWidget(FontBtn,2,0);
    layout->addWidget(FontLineEdit,2,1);
    connect(FontBtn,SIGNAL(clicked(bool)),
            this,SLOT(FontDlg()));

    //inputBtn
    inputBtn= new QPushButton(this);
    inputBtn->setText("标准输出对话框");
    layout->addWidget(inputBtn,3,0);
    connect(inputBtn,SIGNAL(clicked(bool)),
            this,SLOT(InputDlg()));

    MsgBtn= new QPushButton(this);
    MsgBtn->setText("标准消息对话框");
    layout->addWidget(MsgBtn,3,1);
    connect(MsgBtn,SIGNAL(clicked(bool)),
            this,SLOT(MsgDlg()));
}

Dialog::~Dialog(){}

   void Dialog::FileDlg()
   {
   QString str = QFileDialog::getOpenFileName(
                this,"标准文件对话框","./",
                 "txt文件(*.txt *.cpp)"
                );
   fileLineEdit->setText(str);
}

 void Dialog::ColorDlg()
 {
    QColor qc= QColorDialog::getColor(Qt::red,this);
    if(qc.isValid())
    {
        colorFrame->setPalette(QPalette(qc));
    }
 }


void Dialog::FontDlg()
{
    bool ok;
    QFont qft = QFontDialog::getFont(&ok,this);
    if(ok)
    {
        FontLineEdit->setFont(qft);
    }

}

void Dialog::InputDlg()
{
    QStringList str;
    str << "苍姐姐"<<"芙蓉姐姐"<<"广广哥";
    bool ok;
    //标准int输入框
    // QInputDialog::getInt(this,"int输入框","输入年龄",17,0,150,2,&ok);
    //  QInputDialog::getDouble(this,"double输入框","输入长度",12.12,3.0,18.00,2,&ok);
    //QInputDialog::getItem(this,"翻牌","翻我吧",str,1,true/*设置是否可编辑*/,&ok);
    QInputDialog::getText(this,"字符串","字符串",QLineEdit::Password,"xxxx",&ok);
}

void Dialog::MsgDlg()
{
   //QMessageBox::aboutQt(this);
   switch( QMessageBox::question(this,"没事干",
                          "没事干我就去学习(苍姐姐)",
                          QMessageBox::Ok|QMessageBox::Cancel))
   {
case QMessageBox::Ok:
       QMessageBox::aboutQt(this);
       break;
      case QMessageBox::Cancel:
       QMessageBox::about(this,"about","xxxxx");
       break;
   }
  // QMessageBox::information(this,"消息","测试");
   QMessageBox::critical(this,"选择我吧","我就是我");
}
