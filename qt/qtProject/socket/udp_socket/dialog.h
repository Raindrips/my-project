#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QUdpSocket>
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    QUdpSocket* uSocket;
private:
    Ui::Dialog *ui;
public slots:
    void SendMsg();
};

#endif // DIALOG_H
