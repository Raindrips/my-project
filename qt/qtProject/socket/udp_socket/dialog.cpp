#include "dialog.h"
#include "ui_dialog.h"
const quint16 port = 6666;
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    //1.1 初始化套接字
    uSocket = new QUdpSocket(this);
    //1.2 处理函数
    connect(ui->pushButton,SIGNAL(clicked(bool)),
            this,SLOT(SendMsg()));

}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::SendMsg()
{
    QString msg = ui->lineEdit->text();
    //toLatin1 ascii编码方式
    uSocket->writeDatagram(
                msg.toLatin1(),msg.length(),
     QHostAddress::Broadcast       /* QHostAddress("127.0.0.1")*/,port);
}
