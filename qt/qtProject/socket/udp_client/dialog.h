#ifndef DIALOG_H
#define DIALOG_H

//这个是服务器端.名字写错了
#include <QDialog>
#include <QUdpSocket>
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    QUdpSocket* udpSocket;
private:
    Ui::Dialog *ui;
public slots:
   void ReadMsg();
};

#endif // DIALOG_H
