#include "widget.h"
#include <QDebug>
#include <QNetworkInterface>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    hostInfo();
}

Widget::~Widget()
{

}

void Widget::hostInfo()
{
    //1.1获取主机名
   QString hostName =  QHostInfo::localHostName();
  // QHostInfo::lookupHost("www.tanzhouedu.com",
   //this,printInfo(QHostInfo));
   qDebug()<<hostName.toStdString().data();

   //1.2 主机名获取ip地址
    QHostInfo hif= QHostInfo::fromName(hostName);
    QList<QHostAddress> listAddr= hif.addresses();
    if(!listAddr.isEmpty())
    {//128 ipv6
        qDebug()<< listAddr.first().toString().toStdString().data();
    }
    //1.3 用来获取主机的ip地址 或者网络接口的列表
   QList<QNetworkInterface> nlist=
           QNetworkInterface::allInterfaces();
   for(int i = 0; i < nlist.count();++i)
   {
     QNetworkInterface temp =  nlist.at(i);
     qDebug()<< "设备名:"<<temp.name();
     qDebug()<< "MAC:"<< temp.hardwareAddress();
     //temp.allAddresses();
     //1.4 获取硬件所有信息
     QList<QNetworkAddressEntry> listEntry =
             temp.addressEntries();
     for(int j = 0; j < listEntry.count();++j)
     {
        QNetworkAddressEntry t =listEntry.at(j);
         qDebug()<< "ip"<<t.ip().toString();
          qDebug()<< "子网掩码:"<<t.netmask() .toString();
     }
   }
}
