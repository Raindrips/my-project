#-------------------------------------------------
#
# Project created by QtCreator 2017-11-23T20:43:38
#
#-------------------------------------------------

QT       += core gui
QT       += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = socketInfo
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
