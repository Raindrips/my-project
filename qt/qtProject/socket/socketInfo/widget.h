#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QHostInfo>
class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();
     void hostInfo();
};

#endif // WIDGET_H
