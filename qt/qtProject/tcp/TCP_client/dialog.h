#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTcpSocket>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    QTcpSocket* tcpSocket;
private:
    Ui::Dialog *ui;
public slots:
    void ConnectServer();
    void readMsg();
    void showError(QAbstractSocket::SocketError);
    void sendMsg();
};

#endif // DIALOG_H
