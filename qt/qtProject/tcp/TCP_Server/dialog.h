#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTcpServer>
#include <QTcpSocket>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    QTcpServer * server;
    QTcpSocket * socket;

private:
    Ui::Dialog *ui;

public slots:
    void startServer();
    void connectServer();
    void recvData();
    void sendData();
};

#endif // DIALOG_H
