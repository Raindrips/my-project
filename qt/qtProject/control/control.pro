#-------------------------------------------------
#
# Project created by QtCreator 2017-10-31T20:54:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = control
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
