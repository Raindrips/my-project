#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QDateEdit>
#include <QDial>
#include <QSpinBox>
#include <QLCDNumber>
#include <QProgressBar> //进度条
#include <QScrollBar>
class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();
public:
    QLineEdit * pLoginEdit;
    QDateEdit* pDateEdit;
    QDial* pQDial;
    QSpinBox* pSpinBox;
    QLCDNumber* pLcdNumber;
    QProgressBar* pProgressBar;
    QScrollBar* pScroll;
public:
    void useEdit();
    void useDial();
    void useSpinBox();
    void useLcdNumber();
    void useProgessBar();
public slots:
    void dtChange(QDate da);
    void OnChange(int va);

    void OntimeOut();

};

#endif // WIDGET_H
