#include "widget.h"
#include <QDate>
#include <QDebug>
#include <QTimeEdit>
#include <QTimer>
// 编辑框
// 数码框
// 表盘
// 微调框
// 进度条
// 滑动条
Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    setMinimumSize(500,300);
    setMaximumSize(500,300);
   // useEdit();
    useDial();
    useSpinBox();
  //  useLcdNumber();
    useProgessBar();

}

Widget::~Widget()
{

}

 void Widget::useEdit()
 {
    pLoginEdit = new QLineEdit(this);
    pLoginEdit->setGeometry(10,10,100,30);
    //在输入之前提示.
    pLoginEdit->setPlaceholderText(QString("数字,字母,$"));
    //密码框
    pLoginEdit->setEchoMode(QLineEdit::Password);
    //设文本
    pLoginEdit->setText(QString("danny"));
    //设置只读
   // pLoginEdit->setReadOnly(true);
    //设置最大长度
    pLoginEdit->setMaxLength(5);

    //
    QDate da = QDate(1998,11,30);
    //QDateEdit 时间加上2年
    pDateEdit = new QDateEdit(QDate::currentDate() ,this);
    pDateEdit->setGeometry(10,70,200,30);

    //设在显示格式
    pDateEdit->setDisplayFormat("MM-dd-yyyy");
    //弹出日期框
    pDateEdit->setCalendarPopup(true);

    //dateChanged()
    connect(pDateEdit,SIGNAL(dateChanged(QDate)),this,SLOT(dtChange(QDate)));

    QTimeEdit * ptemp = new QTimeEdit(QTime::currentTime() ,this);
    ptemp->setGeometry(10,120,200,30);

 }

  void Widget::dtChange(QDate da)
  {

   qDebug("日期处理函数%s",da.currentDate().toString().toStdString().data());
  }

  void  Widget::useDial()
  {
    pQDial = new QDial(this);
    pQDial->setGeometry(10,20,200,200);
    //设置刻度
    pQDial->setNotchesVisible(true);
    //设置范围
    pQDial->setRange(0,200);
    //设置循环
    pQDial->setWrapping(true);
    //设在位置
    pQDial->setValue(110);
    connect(pQDial,SIGNAL(valueChanged(int)),this,SLOT(OnChange(int)));
  }

void Widget::OnChange(int va)
{
    qDebug() << "数值改变"<<va;
    pSpinBox->setValue(va);
    pProgressBar->setValue(va);
    pProgressBar->setFormat(QString("已经安装%1%").arg(va/2));
    pScroll->setValue(va);
}

void Widget::useSpinBox()
{
    pSpinBox = new QSpinBox(this);
    pSpinBox->setGeometry(10,20,200,20);
    pSpinBox->setRange(0,200);
    pSpinBox->setValue(100);
    //加前缀
    pSpinBox->setPrefix("$");

    //设置文本居中
    pSpinBox->setAlignment(Qt::AlignCenter);

    //加后缀
   pSpinBox->setSuffix("%");

   //设置循环
    pSpinBox->setWrapping(true);
    //槽函数处理方式和仪表盘一样

}

 void Widget::useLcdNumber()
 {
    pLcdNumber = new QLCDNumber(this);
    pLcdNumber->setGeometry(10,20,400,50);
    //显示数据
    pLcdNumber->display("12345");
    //设置进制
    pLcdNumber->setMode(QLCDNumber::Dec);
    //设置风格 设置平面型
    pLcdNumber->setSegmentStyle(QLCDNumber::Flat);
   //可以上颜色
    //设置显示位数
    pLcdNumber->setDigitCount(30);
    //定时器
    QTimer * pt = new QTimer(this);
    pt->setInterval(1000);
    connect(pt,SIGNAL(timeout()),this,SLOT(OntimeOut()));
    pt->start();
 }

 void Widget::OntimeOut()
 {
     QTime p = QTime::currentTime();
     pLcdNumber->display(p.toString("HH:mm:ss") );
 }

 void Widget::useProgessBar()
 {
    pProgressBar = new QProgressBar(this);
    //设在垂直
   // pProgressBar->setOrientation(Qt::Vertical);
    pProgressBar->setGeometry(10,220,400,30);
    pProgressBar->setRange(0,200);
    pProgressBar->setValue(10);
    //设置显示的内容
    pProgressBar->setFormat(QString("已经安装..."));
    //设置方向
    pProgressBar->setInvertedAppearance(true);

    pScroll = new QScrollBar(this);
    pScroll->setGeometry(250,100,40,300);

 }
