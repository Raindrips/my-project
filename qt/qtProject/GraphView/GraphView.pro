#-------------------------------------------------
#
# Project created by QtCreator 2017-11-12T20:50:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GraphView
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mygriaphitem.cpp

HEADERS  += mainwindow.h \
    mygriaphitem.h
