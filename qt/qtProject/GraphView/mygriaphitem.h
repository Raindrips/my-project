#ifndef MYGRIAPHITEM_H
#define MYGRIAPHITEM_H

#include <QObject>
#include <QGraphicsItem>
#include <QRectF>

class MyGriaphItem : public QObject,public QGraphicsItem
{
    Q_OBJECT
public:
    explicit MyGriaphItem(QObject *parent = 0);
    //设在图元区域
    QRectF boundingRect() const ;

    void     paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR) ;

    void timerEvent(QTimerEvent* p);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // MYGRIAPHITEM_H
