#include "mygriaphitem.h"
#include <QPainter>
#include <QDebug>
#include <QCursor>
#include <QGraphicsSceneMouseEvent>
#include <QDrag>

MyGriaphItem::MyGriaphItem(QObject *parent) : QObject(parent)
{
    //设置定时器
    startTimer(1000);
    //设置提示
    setToolTip(QString("我爱学习同学--翻拍"));
    //换图标
    setCursor(Qt::OpenHandCursor);

}

QRectF MyGriaphItem::boundingRect() const
{
    return QRectF(10,10,100,100);
}

void     MyGriaphItem::paint(QPainter *painter,
                             const QStyleOptionGraphicsItem *option,
                             QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->setBrush(Qt::green);
    painter->drawRect(20,20,100,100);
}


 void MyGriaphItem::timerEvent(QTimerEvent* p)
 {
     Q_UNUSED(p);
     static int x = 0;
     x++;
     qDebug() << QString("我是第:").toStdString().data()
              <<x << QString("次").toStdString().data();
 }

void MyGriaphItem::mousePressEvent(
        QGraphicsSceneMouseEvent *event)
{
    static int s = 1;
    if(event->button() == Qt::LeftButton )
    {
        if(s){
          setCursor(Qt::ClosedHandCursor);
          s = 0;
        }else
        {
         //   Qdop
             setCursor(Qt::SizeAllCursor );
             s = 1;
        }
    }
}
