﻿#include "dialog.h"
#include "ui_dialog.h"

#include <QIcon>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    this->setWindowIcon(QIcon(""));

}

Dialog::~Dialog()
{
    delete ui;
}
