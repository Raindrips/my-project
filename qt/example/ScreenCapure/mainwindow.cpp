﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QScreen>
#include <QDesktopWidget>
#include <QDebug>
#include <QBuffer>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    this->captureScreen();
}

//屏幕截图
void MainWindow::captureScreen()
{
    QScreen *screen = QApplication::primaryScreen(); //获取当前程序的主窗口对象
    QPixmap map = screen->grabWindow(QApplication::desktop()->winId()); //调用主窗口对象的捕捉窗口图像，并传递桌面窗口的id号
    bool flag=map.save("./1.png"); //保存图像
    qDebug()<<flag;

    QByteArray ba;
    QBuffer    bf(&ba);
    // 30表示压宿率，值从0 – 100, 值越小表示编码出来的图像文件就越小，当然也就越不清晰
    map.save(&bf, "png", 30);

}
