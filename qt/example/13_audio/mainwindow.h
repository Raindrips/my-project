﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAudioProbe>
#include <QAudioRecorder>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void openFile();

protected slots:
    void onAudioBufferProbed(const QAudioBuffer& buffer);

    void onStateChanged(QMediaRecorder::State state);

    void onDurationChanged(qint64 duration);

private slots:
    void on_actionRecord_triggered();

    void on_pushButton_clicked();

    void on_actionStop_triggered();

    void on_actionPause_triggered();

    void on_actionQuit_triggered();


    void on_pushButton_file_clicked();

private:
    Ui::MainWindow *ui;

    QAudioRecorder *recorder;//音频录音
    QAudioProbe *probe; //探测器

    void init();

    void initAudio();


};
#endif // MAINWINDOW_H
