﻿#ifndef VIDIOWIDGET_H
#define VIDIOWIDGET_H

#include <QMediaPlayer>
#include <QVideoWidget>


class VideoWidget : public QVideoWidget
{
public:
    VideoWidget(QWidget *parent = nullptr);
    virtual ~VideoWidget();

    void setMediaPlayer(QMediaPlayer *player);

protected:

    void keyPressEvent(QKeyEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;
private:
    //播放类
    QMediaPlayer    *thePlayer;
};

#endif // VIDIOWIDGET_H
