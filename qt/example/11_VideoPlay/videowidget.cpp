﻿
#include "videowidget.h"
#include <QKeyEvent>
#include <QMediaPlayer>



VideoWidget::VideoWidget(QWidget *parent)
{

}

VideoWidget::~VideoWidget()
{

}

void VideoWidget::setMediaPlayer(QMediaPlayer *player)
{
    this->thePlayer=player;
}

void VideoWidget::keyPressEvent(QKeyEvent *event)
{
    QVideoWidget::keyPressEvent(event);
    //按键事件，ESC退出全屏状态
    if ((event->key() == Qt::Key_Escape)&&(this->isFullScreen()))
    {
        this->setFullScreen(false);
        event->accept();
    }

}

void VideoWidget::mousePressEvent(QMouseEvent *event)
{
    QVideoWidget::mousePressEvent(event);
    //鼠标事件，单击控制暂停和继续播放
    if (event->button()==Qt::LeftButton)
    {
        if (thePlayer->state()==QMediaPlayer::PlayingState){
            thePlayer->pause();
        }
        else{
            thePlayer->play();
        }
    }

}
