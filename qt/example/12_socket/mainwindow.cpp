﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QHostInfo>
#include <QInputDialog>
#include <QNetworkInterface>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::lookedUpHostInfo(const QHostInfo &hostInfo)
{
    //设置主机名
    QList<QHostAddress> addList=hostInfo.addresses();//IP地址列表
    if (!addList.isEmpty()){
        for (int i=0;i<addList.count();i++)
        {
            QHostAddress aHost=addList.at(i);

            //协议类型
            ui->plainTextEdit->appendPlainText("协议:"+
                                               this->protocolName(aHost.protocol()));
            //IP地址
            ui->plainTextEdit->appendPlainText("本机IP地址："+aHost.toString());
            ui->plainTextEdit->appendPlainText("\n");
        }
    }
}
void MainWindow::on_actionhostName_triggered()
{
    QString hostName = QHostInfo::localHostName();
    this->ui->plainTextEdit->appendPlainText("本机主机名:"+hostName+"\n");
}

void MainWindow::on_actionIPAdressed_triggered()
{
    QHostInfo   hostInfo=QHostInfo::fromName( QHostInfo::localHostName());
    this->lookedUpHostInfo(hostInfo);
}

QString MainWindow::protocolName(QAbstractSocket::NetworkLayerProtocol protocol)
{
    switch(protocol)
    {
    case QAbstractSocket::IPv4Protocol:
        return "IPv4 Protocol";
    case QAbstractSocket::IPv6Protocol:
        return "IPv6 Protocol";
    case QAbstractSocket::AnyIPProtocol:
        return "Any IP Protocol";
    default:
        return "Unknown Network Layer Protocol";
    }
}

void MainWindow::on_actionfindIPAddresses_triggered()
{
    QString title="请输入ip地址";
    QString hostName =QInputDialog::getText(this,title,"");

    if(!hostName.isEmpty()){
        ui->plainTextEdit->appendPlainText("正在查找主机信息："+hostName);
        int ret = QHostInfo::lookupHost(hostName,this,SLOT(lookedUpHostInfo(QHostInfo)));
        qDebug()<<ret;
    }
}

void MainWindow::on_pushButton_clicked()
{
    this->ui->plainTextEdit->clear();
}

void MainWindow::on_actionallAddress_triggered()
{
    //通过QNetworkInterface获取IP地址
    QList<QHostAddress> addList=QNetworkInterface::allAddresses();
    if (!addList.isEmpty())
        for (int i=0;i<addList.count();i++)
        {
            QHostAddress aHost=addList.at(i);
            ui->plainTextEdit->appendPlainText("协  议："+protocolName(aHost.protocol()));
            ui->plainTextEdit->appendPlainText("IP地址："+aHost.toString());
            ui->plainTextEdit->appendPlainText("");
        }
}

void MainWindow::on_actionallInterfaces_triggered()
{
    //通过QNetworkInterface获取设备信息
    QList<QNetworkInterface>    list=QNetworkInterface::allInterfaces();
    for(int i=0;i<list.count();i++)
    {
        QNetworkInterface aInterface=list.at(i);
        if (!aInterface.isValid())
            continue;

        ui->plainTextEdit->appendPlainText("设备名称："+aInterface.humanReadableName());
        ui->plainTextEdit->appendPlainText("硬件地址："+aInterface.hardwareAddress());

        QList<QNetworkAddressEntry> entryList=aInterface.addressEntries();
        for(int j=0;j<entryList.count();j++)
        {
            QNetworkAddressEntry aEntry=entryList.at(j);
            ui->plainTextEdit->appendPlainText("\tIP 地址："+aEntry.ip().toString());
            ui->plainTextEdit->appendPlainText("\t子网掩码："+aEntry.netmask().toString());
            ui->plainTextEdit->appendPlainText("\t广播地址："+aEntry.broadcast().toString()+"\n");
        }
        ui->plainTextEdit->appendPlainText("\n");
    }
}
