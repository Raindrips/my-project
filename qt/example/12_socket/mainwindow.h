﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAbstractSocket>
#include <QMainWindow>

class QHostInfo;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void lookedUpHostInfo(const QHostInfo &host);

    void on_actionhostName_triggered();

    void on_actionIPAdressed_triggered();

    void on_actionfindIPAddresses_triggered();

    void on_pushButton_clicked();

    void on_actionallAddress_triggered();

    void on_actionallInterfaces_triggered();

private:
    Ui::MainWindow *ui;

     QString  protocolName(QAbstractSocket::NetworkLayerProtocol protocol);
};
#endif // MAINWINDOW_H
