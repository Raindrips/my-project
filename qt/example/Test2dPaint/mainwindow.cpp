﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QBitmap>
#include <QDebug>
#include <QLabel>
#include <QMouseEvent>
#include <QPaintDevice>
#include <QPainter>
#include <QPicture>
#include <QToolTip>
#include <QWindow>
#include <QScreen>
#include <QApplication>
#include <QDesktopWidget>
#include <QTime>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setMouseTracking(true);

    //计时器
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);
    angle = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    this->test1();
    this->test2();
    this->test3();
    this->test4();
    this->test5();
    this->test6();
    this->test7();
    this->test9();
}

void MainWindow::test1()
{
    //绘制类
    QPainter *painter = new QPainter(this);
    painter->drawLine(QPoint(0, 0), QPoint(100, 100));

    // 创建画笔,使用画笔
    QPen pen(Qt::green, 5, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin);
    pen.setWidth(10);
    painter->setPen(pen);

    // 绘制圆弧
    QRectF rect(70,70,80,80);
    int startAngle=0;
    int spanAngle=360*16;
    painter->drawArc(rect,startAngle,spanAngle);

    // 绘制椭圆
    painter->drawEllipse(220,20,50,50);

    //创建画刷
    QBrush brush(QColor(0,0,255),Qt::Dense1Pattern);
    //设置画刷材质
    brush.setTexture(QPixmap("../*.png"));
    //设置画刷
    painter->setBrush(brush);
    // 定义四个点
    static const QPointF points[4] = {
        QPointF(270.0, 80.0),
        QPointF(290.0, 10.0),
        QPointF(350.0, 30.0),
        QPointF(390.0, 70.0),
    };

    // 使用四个点绘制多边形
    painter->drawPolygon(points, 4);

    // 使用画刷填充一个矩形区域
    painter->fillRect(QRect(10, 100, 150, 20), QBrush(Qt::darkYellow));

    // 擦除一个矩形区域的内容
    painter->eraseRect(QRect(50, 0, 50, 120));

    // 线性渐变
    QLinearGradient linearGradient(QPointF(40, 190), QPointF(70, 190));
    // 插入颜色
    linearGradient.setColorAt(0.1, Qt::yellow);
    linearGradient.setColorAt(0.5, Qt::red);
    linearGradient.setColorAt(1, Qt::green);

    // 指定渐变区域以外的区域的扩散方式
    linearGradient.setSpread(QGradient::RepeatSpread);

    // 使用渐变作为画刷
    painter->setBrush(linearGradient);
    painter->drawRect(10, 170, 90, 40);

    // 辐射渐变
    QRadialGradient radialGradient(QPointF(200, 190), 50, QPointF(275, 200));
    radialGradient.setColorAt(0, QColor(255, 255, 100, 150));
    radialGradient.setColorAt(1, QColor(0, 0, 0, 50));
    painter->setBrush(radialGradient);
    painter->drawEllipse(QPointF(200, 190), 50, 50);

    // 锥形渐变
    QConicalGradient conicalGradient(QPointF(350, 190), 60);
    conicalGradient.setColorAt(0.2, Qt::cyan);
    conicalGradient.setColorAt(0.9, Qt::black);
    painter->setBrush(conicalGradient);
    painter->drawEllipse(QPointF(350, 190), 50, 50);

    // 画笔使用线性渐变来绘制直线和文字
    painter->setPen(QPen(linearGradient,2));
    painter->drawLine(0, 280, 100, 280);
    painter->drawText(150, 280, tr("helloQt!"));

}

void MainWindow::test2()
{
    QPainter painter(this);
    // 填充界面背景为白色
    painter.fillRect(rect(), Qt::white);
    painter.setPen(QPen(Qt::red, 11));
    // 绘制一条线段
    painter.drawLine(QPoint(5, 6), QPoint(100, 99));
    // 将坐标系统进行平移，使(200, 150)点作为原点
    painter.translate(200, 150);
    // 开启抗锯齿
    painter.setRenderHint(QPainter::Antialiasing);
    // 重新绘制相同的线段
    painter.drawLine(QPoint(5, 6), QPoint(100, 99));

    // 保存painter的状态
    painter.save();
    // 将坐标系统旋转90度
    painter.rotate(90);
    painter.setPen(Qt::cyan);
    // 重新绘制相同的线段
    painter.drawLine(QPoint(5, 6), QPoint(100, 99));
    // 恢复painter的状态
    painter.restore();

    //
    painter.setBrush(Qt::darkGreen);
    // 绘制一个矩形
    painter.drawRect(-50, -50, 100, 50);
    painter.save();
    // 将坐标系统进行缩放
    painter.scale(0.5, 0.4);
    painter.setBrush(Qt::yellow);
    // 重新绘制相同的矩形
    painter.drawRect(-50, -50, 100, 50);
    painter.restore();

    // 设置画刷qt::blue
    painter.setPen(Qt::blue);
    painter.setBrush(Qt::darkYellow);
    // 绘制一个椭圆
    painter.drawEllipse(QRect(60, -100, 50, 50));
    // 将坐标系统进行扭曲
    painter.shear(1.5, -0.7);
    painter.setBrush(Qt::darkGray);
    // 重新绘制相同的椭圆
    painter.drawEllipse(QRect(60, -100, 50, 50));

}

void MainWindow::test3()
{
    //    QPainter painter(this);
    //    painter.setWindow(-50, -50, 100, 100);
    //    painter.setBrush(Qt::green);
    //    painter.drawRect(0, 0, 20, 20);

    QPainter painter(this);
    int side = qMin(width(), height());
    int x = (width() / 2);
    int y = (height() / 2);
    // 设置视口
    painter.setViewport(x, y, side, side);
    painter.setWindow(0, 0, 100, 100);
    painter.setBrush(Qt::green);
    painter.drawRect(0, 0, 20, 20);
}

void MainWindow::test4()
{
    QPainter painter(this);
    QRectF rect(10.0, 10.0, 380.0, 280.0);
    painter.setPen(Qt::red);
    painter.drawRect(rect);
    painter.setPen(Qt::blue);
    //绘制文本
    painter.drawText(rect, Qt::AlignHCenter, tr("AlignHCenter"));
    painter.drawText(rect, Qt::AlignLeft, tr("AlignLeft"));
    painter.drawText(rect, Qt::AlignRight, tr("AlignRight"));
    painter.drawText(rect, Qt::AlignVCenter, tr("AlignVCenter"));
    painter.drawText(rect, Qt::AlignBottom, tr("AlignBottom"));
    painter.drawText(rect, Qt::AlignCenter, tr("AlignCenter"));
    painter.drawText(rect, Qt::AlignBottom | Qt::AlignRight,
                     tr("AlignBottom\nAlignRight"));

    QFont font("宋体", 15, QFont::Bold, true);
    //设置下划线
    font.setUnderline(true);
    //设置上划线
    font.setOverline(true);
    //设置字母大小写
    font.setCapitalization(QFont::SmallCaps);
    //设置字符间的间距
    font.setLetterSpacing(QFont::AbsoluteSpacing, 10);
    //使用字体
    painter.setFont(font);
    painter.setPen(Qt::green);
    painter.drawText(120, 80, tr("tanzhoujiaoyu"));

    painter.translate(100, 100);
    painter.rotate(90);
    painter.drawText(0, 0, tr("helloqt"));
}

void MainWindow::test5()
{
    QPainter painter(this);

    //绘制路径
    QPainterPath path;
    // 移动当前点到点(50, 250)
    path.moveTo(50, 250);
    // 从当前点即(50, 250)绘制一条直线到点(50, 230)，完成后当前点更改为(50, 230)
    path.lineTo(50, 230);
    // 从当前点和点(120, 60)之间绘制一条三次贝塞尔曲线
    path.cubicTo(QPointF(105, 40), QPointF(115, 80), QPointF(120, 60));
    path.lineTo(130, 130);
    // 向路径中添加一个椭圆
    path.addEllipse(QPoint(130, 130), 30, 30);

    painter.setPen(Qt::darkYellow);
    // 绘制路径
    painter.drawPath(path);

    // 平移坐标系统后重新绘制路径
    path.translate(200,0);
    painter.setPen(Qt::darkBlue);
    painter.drawPath(path);
}

void MainWindow::test6()
{
    QPainter painter(this);
    //绘制路径
    QPainterPath path;
    path.addEllipse(10,50,100,100);
    path.addRect(50,100,100,100);
    painter.setBrush(Qt::cyan);
    painter.drawPath(path);

    painter.translate(180,0);
    path.setFillRule(Qt::WindingFill);
    painter.drawPath(path);
}

void MainWindow::test7()
{
    QPainter painter;
    // 绘制image
    QImage image(100, 100, QImage::Format_ARGB32);
    painter.begin(&image);
    painter.setPen(QPen(Qt::green, 3));
    painter.setBrush(Qt::yellow);
    painter.drawRect(10, 10, 60, 60);
    painter.drawText(10, 10, 60, 60, Qt::AlignCenter, tr("QImage"));
    painter.setBrush(QColor(0 , 0, 0, 100));
    painter.drawRect(50, 50, 40, 40);
    painter.end();
    // 绘制pixmap
    QPixmap pix(100, 100);
    painter.begin(&pix);
    painter.setPen(QPen(Qt::green, 3));
    painter.setBrush(Qt::yellow);
    painter.drawRect(10, 10, 60, 60);
    painter.drawText(10, 10, 60, 60, Qt::AlignCenter, tr("QPixmap"));
    painter.setBrush(QColor(0 , 0, 0, 100));
    painter.drawRect(50, 50, 40, 40);
    painter.end();
    // 绘制bitmap
    QBitmap bit(100, 100);
    painter.begin(&bit);
    painter.setPen(QPen(Qt::green, 3));
    painter.setBrush(Qt::yellow);
    painter.drawRect(10, 10, 60, 60);
    painter.drawText(10, 10, 60, 60, Qt::AlignCenter, tr("QBitmap"));
    painter.setBrush(QColor(0 , 0, 0, 100));
    painter.drawRect(50, 50, 40, 40);
    painter.end();
    // 绘制picture
    QPicture picture;
    painter.begin(&picture);
    painter.setPen(QPen(Qt::green, 3));
    painter.setBrush(Qt::yellow);
    painter.drawRect(10, 10, 60, 60);
    painter.drawText(10, 10, 60, 60, Qt::AlignCenter, tr("QPicture"));
    painter.setBrush(QColor(0 , 0, 0, 100));
    painter.drawRect(50, 50, 40, 40);
    painter.end();
    // 在widget部件上进行绘制
    painter.begin(this);
    painter.drawImage(50, 20, image);
    painter.drawPixmap(200, 20, pix);
    painter.drawPixmap(50, 170, bit);
    painter.drawPicture(200, 170, picture);
}

void MainWindow::test8()
{
    QPainter painter(this);
    QImage image;
    // 加载一张图片
    image.load("../image.png");
    // 输出图片的一些信息
    qDebug() << image.size() << image.format() << image.depth();
    // 在界面上绘制图片
    painter.drawImage(QPoint(10, 10), image);
    // 获取镜像图片
    QImage mirror = image.mirrored();
    // 将图片进行扭曲
    QTransform transform;
    transform.shear(0.2, 0);
    QImage image2 = mirror.transformed(transform);
    painter.drawImage(QPoint(10, 160), image2);
    // 将镜像图片保存到文件
    image2.save("../mirror.png");
}

void MainWindow::test9()
{
    QPainter painter;
    QImage image(400, 300, QImage::Format_ARGB32_Premultiplied);
    painter.begin(&image);
    painter.setBrush(Qt::green);
    painter.drawRect(100, 50, 200, 200);
    painter.setBrush(QColor(0, 0, 255, 150));
    painter.drawRect(50, 0, 100, 100);
    painter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    painter.drawRect(250, 0, 100, 100);
    painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);
    painter.drawRect(50, 200, 100, 100);
    painter.setCompositionMode(QPainter::CompositionMode_Xor);
    painter.drawRect(250, 200, 100, 100);
    painter.end();
    painter.begin(this);
    painter.drawImage(0, 0, image);
}

void MainWindow::test10()
{
    QPainter painter(this);
    //绘制图片
    QPixmap pix;
    pix.load("../a.png");
    painter.drawPixmap(0, 0, pix.width(), pix.height(), pix);
    painter.setBrush(QColor(255, 255, 255, 100));
    painter.drawRect(0, 0, pix.width(), pix.height());
    painter.drawPixmap(100, 0, pix.width(), pix.height(), pix);
    painter.setBrush(QColor(0, 0, 255, 100));
    painter.drawRect(100, 0, pix.width(), pix.height());

}

void MainWindow::test11()
{
    QWindow window;
    QPixmap grab =  window.screen()->grabWindow(QApplication::desktop()->winId());
    grab.save("../screen.png");
    QLabel *label = new QLabel(this);
    label->resize(640, 480);
    QPixmap pix = grab.scaled(label->size(),
                              Qt::KeepAspectRatio,
                              Qt::SmoothTransformation);
    label->setPixmap(pix);
    label->move(0, 100);
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    QString pos = QString("%1,%2").arg(event->pos().x()).arg(event->pos().y());
    QToolTip::showText(event->globalPos(), pos, this);
}
