#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QTimer *timer = nullptr;
    int angle = 0;

protected:
    void paintEvent(QPaintEvent *event) override;

    void test1();
    void test2();
    void test3();
    //绘制文本
    void test4();

    void test5();

    //绘制路径
    void test6();

    void test7();

    //加载图片
    void test8();

    void test9();

    void test10();

    void test11();

    void mouseMoveEvent(QMouseEvent *event) override;



private:
    Ui::MainWindow *ui;

};
#endif // MAINWINDOW_H
