﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    Q_PROPERTY(int _age READ age WRITE setAge NOTIFY ageChange)

    Q_CLASSINFO("MainWindows","呵呵")
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    int _age=0;

    int age() const;

    void setAge(int age);
signals:
     void ageChange(int age);

private:
    Ui::MainWindow *ui;


public slots:
    //void run();

};
#endif // MAINWINDOW_H
