﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QStringList>
using namespace std;
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qDebug()<<this->inherits("QObject")<<endl;
    qDebug()<<this->metaObject()->className()<<endl;

    this->setProperty("age",10);
    qDebug()<<this->property("age");
    qDebug()<<this->property("age").toInt();

}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::age()const
{
    return this->_age;
}

void MainWindow::setAge(int age)
{
    this->_age=age;
}

