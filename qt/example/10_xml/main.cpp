﻿#include "mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlResult>
#include <QSqlRecord>

void test1(){
    qDebug() << "Available drivers:";
    QStringList drivers = QSqlDatabase::drivers();
    foreach(QString driver, drivers){
        qDebug() << driver;
    }
}


void test2(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    //设置文件目录
    db.setDatabaseName(QApplication::applicationDirPath()+"/my.db");
    db.open();
    if (!db.isOpen()) {
        qDebug()<<db.lastError().text();
        return;
    }
    QSqlQuery query;
    query.exec("create table student ( "
               "id int primary key,"
               "name varchar(20))");
    query.exec("insert into student values(0, '小明')");
    query.exec("insert into student values(1, '刘涛')");
    query.exec("insert into student values(2, '王红')");
    query.exec("select * from student");
    const QSqlRecord& record= query.record();
    for(int i=0;i<record.count();i++){
        qDebug()<<record.fieldName(i)<<record.value(i);
    }
    //查询表格内容
    query.exec("select * from student");
    while(query.next())
    {
        qDebug() << query.value(0).toInt() << query.value(1).toString();
    }
    db.close();
}


void test3(){
     QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
     db.open();
     if (!db.isOpen()) {
         qDebug()<<db.lastError().text();
         return;
     }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
//    test1();
//    test2();
//    test3();
    return a.exec();
}
