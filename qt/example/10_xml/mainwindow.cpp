﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSqlRelationalDelegate>
#include <QSqlTableModel>
#include <qsqlrelationaltablemodel.h>
#include <QTableView>
#include <bits/concept_check.h>
#include <bits/concept_check.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    //设置文件目录
    db.setDatabaseName(QApplication::applicationDirPath()+"/my.db");
    db.open();
    QSqlQueryModel *model = new QSqlQueryModel(this);

    model->setQuery("select * from student");
    model->setHeaderData(0, Qt::Horizontal, tr("学号"));
    model->setHeaderData(1, Qt::Horizontal, tr("姓名"));
    model->setHeaderData(2, Qt::Horizontal, tr("课程"));

    QTableView *view = new QTableView(this);
    view->setModel(model);
    this->setCentralWidget(view);
}

MainWindow::~MainWindow()
{
    delete ui;
}

