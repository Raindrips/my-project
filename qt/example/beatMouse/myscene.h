﻿#ifndef MYSCENE_H
#define MYSCENE_H

#include <QGraphicsScene>
#include <vector>
#include "myitem.h"

class MyScene : public QGraphicsScene
{
    Q_OBJECT
public:
    MyScene(QObject *parent = nullptr);
    void showMouse();
    void startGame();
    void pauseGame();
    void stopGame();

private:
    QTimer *timer;
    std::vector<MyItem*> items;
    const quint64 COUNT=16;
};

#endif // MYSCENE_H
