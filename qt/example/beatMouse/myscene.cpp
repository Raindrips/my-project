﻿#include "myscene.h"

#include <QTimer>

using namespace std;

MyScene::MyScene(QObject *parent)
    : QGraphicsScene(parent)
{
    for(size_t i=0;i<COUNT;i++){
        auto item=new MyItem();
        item->setPos(i%4*item->
                     boundingRect().width(),i/4*item->
                     boundingRect().height());
        this->addItem(item);
        this->items.push_back(item);
    }
    this->timer=new QTimer();
    this->connect(this->timer,&QTimer::timeout,this,&MyScene::showMouse);
}

void MyScene::showMouse()
{
    size_t count=qrand()%3+1;
    for(size_t i=0;i<this->items.size();i++){
        this->items[i]->setpic(":/pic/map.jpg");
        this->items[i]->setMouse(false);
    }

    for(size_t i=0;i<count;i++){
        size_t index=size_t(qrand())%this->items.size();
        this->items[index]->setpic(":/pic/mouse.jpg");
        this->items[index]->setMouse(true);
    }
}

void MyScene::startGame()
{
    for(size_t i=0;i<this->items.size();i++){
        this->items[i]->setStart(true);
    }
    //开启定时器
    this->timer->start(1000);
}

void MyScene::pauseGame()
{
    this->timer->stop();
    for(size_t i=0;i<this->items.size();i++){
        this->items[i]->setStart(false);
    }
}

void MyScene::stopGame()
{
    for(size_t i=0;i<this->items.size();i++){
        this->items[i]->setStart(false);
    }
    this->timer->stop();
    for(size_t i=0;i<this->items.size();i++){
        this->items[i]->setpic(":/pic/map.jpg");
        this->items[i]->setMouse(false);
    }
}
