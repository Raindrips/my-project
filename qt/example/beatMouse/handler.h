﻿#ifndef HANDLE_H
#define HANDLE_H

#include <QObject>



class Handler:public QObject
{
    Q_OBJECT
public:
    static Handler *getInstance();
    void addSoure();
signals:
    void beatMouse();
private:
    explicit Handler(QObject *parent=nullptr);
    static Handler *hand;
};

#endif // HANDLE_H
