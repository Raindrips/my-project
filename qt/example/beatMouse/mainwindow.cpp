﻿#include "handler.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCloseEvent>
#include <QMessageBox>
#include <QTimer>
#include "myscene.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->myScene=new MyScene();
    this->ui->graphicsView->setScene(myScene); //场景放在视图
    this->score=0;
    Handler *hand=Handler::getInstance();
    this->connect(hand,&Handler::beatMouse,this,&MainWindow::updateScore);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    int res=QMessageBox::question(this,"确定","你真的关闭吗？","是","否");
    if(res==1){
        event->ignore();//忽略
    }
}


void MainWindow::on_actionStart_triggered()
{
    this->myScene->startGame();
}

void MainWindow::on_actionPause_triggered()
{
    this->myScene->pauseGame();
}

void MainWindow::on_actionStop_triggered()
{
    this->myScene->stopGame();
    this->score =0;
    this->ui->lcdNumber->display(this->score);
}

void MainWindow::updateScore()
{
    this->score +=10;
    this->ui->lcdNumber->display(this->score);
}
