﻿#ifndef MYITEM_H
#define MYITEM_H

#include <QGraphicsPixmapItem>

#include <QCursor>

//图片类
class MyItem : public QGraphicsPixmapItem
{


public:
    MyItem();

    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

     //设置开始
    void setStart(bool start);

    //判断是否是开始
    bool isStart();

    //设置图片的函数
    void setpic(QString path);

    //判断是否是老鼠
    bool isMouse();
    void setMouse(bool mouse);
protected:

private:
    bool mouse;
    bool start;
    QCursor cursorUp;
    QCursor cursorDown;
};

#endif // MYITEM_H
