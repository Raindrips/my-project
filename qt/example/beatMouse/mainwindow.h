﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H



#include <QMainWindow>
#include <QTimer>
#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MyScene;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow()override;

     void closeEvent(QCloseEvent * event) override;

public slots:
    void on_actionStart_triggered();

    void on_actionPause_triggered();

    void on_actionStop_triggered();

    void updateScore();

private:
    Ui::MainWindow *ui;

    MyScene *myScene;

    int score;
};
#endif // MAINWINDOW_H
