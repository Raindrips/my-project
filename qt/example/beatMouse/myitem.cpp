﻿#include "myitem.h"
#include "handler.h"

#include <QGraphicsSceneMouseEvent>


MyItem::MyItem()
{
    this->setPixmap( QPixmap(":/pic/map.jpg"));
    this->mouse=false;
    this->start=false;
    this->cursorUp=QCursor(QPixmap(":/pic/press.png"));
    this->cursorDown=QCursor(QPixmap(":/pic/down.png"));
    this->setCursor(this->cursorUp);

}

void MyItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->button()){
        this->setCursor(this->cursorDown);
        if(this->isStart()){  //加 判断游戏是否开始
            auto hand=Handler::getInstance();
            if(this->isMouse()){
                hand->addSoure();
                this->setPixmap(QPixmap(":/pic/hit_mouse.jpg"));
                this->mouse=false;
            }
        }
    }

}

void MyItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->button()){
          this->setCursor(this->cursorUp);
    }
}

void MyItem::setStart(bool start)
{
     this->start=start;
}

bool MyItem::isStart()
{
    return start;
}

void MyItem::setpic(QString path)
{
    this->setPixmap(QPixmap(path));
}

bool MyItem::isMouse()
{
    return this->mouse;
}

void MyItem::setMouse(bool mouse)
{
    this->mouse=mouse;
}

