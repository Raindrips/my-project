#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->test();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::test()
{
    this->ui->statusbar->showMessage("message",2000);

    /// 进度条
    auto progressBar= this->ui->progressBar;
    /// 设置范围
    progressBar->setRange(30,70);
    /// 修改值
    progressBar->setValue(60);

}

