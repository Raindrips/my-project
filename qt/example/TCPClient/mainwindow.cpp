﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtNetwork/qhostaddress.h>

#include <QHostInfo>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionlisten_triggered()
{
    //开始监听
    QString IP=ui->comboBox->currentText();//IP地址
    quint16 port=ui->spinBox->value();//端口
    QHostAddress addr(IP);
    tcpServer->listen(addr,port);

}

QString MainWindow::getLocalIP()
{
    //获取本机IPv4地址
    QString hostName=QHostInfo::localHostName();
    //本地主机名
    QHostInfo   hostInfo=QHostInfo::fromName(hostName);
    QString   localIP;

    QList<QHostAddress> addList=hostInfo.addresses();

    if (!addList.isEmpty())
        for (int i=0;i<addList.count();i++)
        {
            QHostAddress aHost=addList.at(i);
            if (QAbstractSocket::IPv4Protocol==aHost.protocol())
            {
                localIP=aHost.toString();
                break;
            }
        }
    return localIP;
}
