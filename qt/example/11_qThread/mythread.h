﻿#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>

class MyThread: public QThread
{

public:
    MyThread();
    // 事件
public:
    bool event(QEvent *event) override;
    // 线程接口
protected:
    void run() override;
};

#endif // MYTHREAD_H
