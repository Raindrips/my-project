﻿#include "mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QThread>
#include "mythread.h"

void test(){
    MyThread *thread=new MyThread();
    thread->start();
    //等待线程结束
    thread->wait();

    delete thread;
    thread=nullptr;
    qDebug()<<"thread end";

    qDebug()<<QThread::idealThreadCount();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    test();
    return a.exec();
}
