﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPainter>
#include <QTime>
#include <QTimer>

const QPoint MainWindow::hourHand[4] = {
    QPoint(3, 5),
    QPoint(0, 13),
    QPoint(-3, 5),
    QPoint(0, -40)
};
const QPoint MainWindow::minuteHand[4] = {
    QPoint(3, 5),
    QPoint(0, 16),
    QPoint(-3, 5),
    QPoint(0, -70)
};
const QPoint MainWindow::secondHand[4] = {
    QPoint(3, 5),
    QPoint(0, 18),
    QPoint(-3, 5),
    QPoint(0, -90)
};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->init();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    //设置透明背景
    // this->setAttribute(Qt::WA_TranslucentBackground, true);
    //最小窗口比例
    this->setWindowFlags(this->windowFlags() | Qt::FramelessWindowHint);

    hourHandPen = QPen(palette().windowText(), 2.0);//设置小时刻度线为粗黑
    minuteHandPen = QPen(palette().windowText(), 1.0);//设置分钟刻度线为灰

    font.setPointSize(10);//字体大小设置为10
    this->setFont(font);

    QTimer *timer = new QTimer(this);
    timer->start(1000);//一秒钟
    this->connect(timer,SIGNAL(timeout()),this,SLOT(update()));
    this->setWindowTitle("The Widget");
    this->resize(360, 360);
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    //设置抗锯齿
    painter.setRenderHint(QPainter::Antialiasing, true);

    int side = qMin(width(), height());
    painter.setViewport((width() - side) / 2, (height() - side) / 2,
                        side, side);
    painter.setWindow(0, 0, 200, 200);
    //重新设定坐标原点
    painter.translate(100,100);
    //绘制
    this->drawClockDial(&painter);
    this->drawHourHand(&painter);
    this->drawMinuteHand(&painter);
    this->drawSecondHand(&painter);
}

void MainWindow::drawHourHand(QPainter *painter)
{
     QTime time = QTime::currentTime();
     painter->setBrush(Qt::black);
     painter->setPen(Qt::black);
     painter->save();
     painter->rotate(30.0*(time.hour()+time.minute()/60.0));
     painter->drawConvexPolygon(hourHand,4);//绘制时针
     painter->restore();//绘制图形后复位坐标系
}

void MainWindow::drawMinuteHand(QPainter *painter)
{
    QTime time = QTime::currentTime();
    painter->setBrush(Qt::blue);
    painter->setPen(Qt::blue);
    painter->save();
    painter->rotate(6.0*(time.minute()+time.second()/60.0));
    painter->drawConvexPolygon(minuteHand,4);//绘制分针
    painter->restore();//绘制图形后复位坐标系
}

void MainWindow::drawSecondHand(QPainter *painter)
{
    QTime time = QTime::currentTime();
    painter->setBrush(Qt::red);
    painter->setPen(Qt::red);
    painter->save();//保存坐标系，防止坐标系跑偏了
    painter->rotate(6.0*time.second());//注意是6.0，不是6
    painter->drawConvexPolygon(secondHand,4);//绘制秒针
    painter->restore();//绘制图形后复位坐标系
}

void MainWindow::drawClockDial(QPainter *painter)
{
    for (int i=0;i<60;i++) {
        painter->save();
        painter->rotate(6*i);
        if(i%5==0){
            painter->setPen(hourHandPen);
            painter->drawLine(0,-100,0,-90);
            painter->drawText(-20, -80, 40, 40,
                              Qt::AlignHCenter |
                              Qt::AlignTop,
                              QString::number(i/5));
        }
        else{
            painter->setPen(minuteHandPen);
            painter->drawLine(0, -98, 0, -95);
        }
        painter->restore();
    }
}

