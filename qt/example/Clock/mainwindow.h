﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPen>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() override;

protected:
    static const QPoint hourHand[4];
    static const QPoint minuteHand[4];
    static const QPoint secondHand[4];
    QPen hourHandPen;
    QPen minuteHandPen;
    QFont font;
private:
    Ui::MainWindow *ui;

    void init();

    //绘制事件
    void paintEvent(QPaintEvent *event) override;
    //绘制时钟
    void drawHourHand(QPainter *painter);
    //绘制分钟
    void drawMinuteHand(QPainter *painter);
    //绘制秒钟
    void drawSecondHand(QPainter *painter);
    //绘制刻度
    void drawClockDial(QPainter *painter);
};

#endif // MAINWINDOW_H
