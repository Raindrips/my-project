#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    //鼠标按下事件
    void mousePressEvent(QMouseEvent *event);
    //鼠标松开事件
    void mouseReleaseEvent(QMouseEvent *event);
    //鼠标双击事件
    void mouseDoubleClickEvent(QMouseEvent *event);
    //鼠标点击移动事件
    void mouseMoveEvent(QMouseEvent *event);
    //滚轮事件
    void wheelEvent(QWheelEvent *event);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
