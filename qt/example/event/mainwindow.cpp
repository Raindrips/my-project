#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLineEdit>
#include <QDebug>
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    QMainWindow::keyPressEvent(event);          // 执行QLineEdit类的默认事件处理
    if(event->type() == QEvent::KeyPress){

        qDebug() << tr("Widget的事件过滤器");
    }
    qDebug() << tr("键盘按下事件");

    if(event->modifiers() == Qt::ControlModifier){ // 是否按下Ctrl键
        if(event->key() == Qt::Key_M)              // 是否按下M键
            setWindowState(Qt::WindowMaximized);   // 窗口最大化
    }
    else QWidget::keyPressEvent(event);

    if (event->key() == Qt::Key_Up) {
        if(event->isAutoRepeat()) return;    // 按键重复时不做处理
        qDebug()<<"keyUp";
    }
    // 忽略该事件
    event->ignore();


}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{

}

void MainWindow::mousePressEvent(QMouseEvent *event)
{

    // 如果是鼠标左键按下
    if(event->button() == Qt::LeftButton){
        QCursor cursor;
        cursor.setShape(Qt::ClosedHandCursor);
        QApplication::setOverrideCursor(cursor); // 使鼠标指针暂时改变形状
        //offset = event->globalPos() - pos();    // 获取指针位置和窗口位置的差值
    }
    // 如果是鼠标右键按下
    else if(event->button() == Qt::RightButton){
        QCursor cursor(QPixmap("../event/res/a.jpg"));
        // 使用自定义的图片作为鼠标指针
        QApplication::setOverrideCursor(cursor);
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    QApplication::restoreOverrideCursor();         // 恢复鼠标指针形状
}

void MainWindow::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton){             // 如果是鼠标左键按下
        if(windowState() != Qt::WindowFullScreen) {
            setWindowState(Qt::WindowFullScreen);      // 将窗口设置为全屏
        }
        else{
            setWindowState(Qt::WindowNoState);        // 否则恢复以前的大小
        }
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    qDebug()<<"mouseMoveEvent"<<event->pos();
}

void MainWindow::wheelEvent(QWheelEvent *event)
{
    qDebug()<<event->delta();
    if(event->delta() > 0){                    // 当滚轮远离使用者时

        ui->textEdit->zoomIn();                // 进行放大
    }else{                                     // 当滚轮向使用者方向旋转时
        ui->textEdit->zoomOut();               // 进行缩小
    }
}

