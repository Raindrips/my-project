#ifndef MAINWINDOWTIME_H
#define MAINWINDOWTIME_H

#include <QMainWindow>

namespace Ui {
class MainWindowTime;
}

class MainWindowTime : public QMainWindow
{
    Q_OBJECT

public:
     int id1, id2, id3;
    explicit MainWindowTime(QWidget *parent = nullptr);
    ~MainWindowTime();

     //时间函数
     void timerEvent(QTimerEvent *event);

     void timerUpdate();

private:
    Ui::MainWindowTime *ui;
};

#endif // MAINWINDOWTIME_H
