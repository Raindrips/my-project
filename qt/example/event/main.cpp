#include "mainwindow.h"
#include "mainwindowtime.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindowTime w;
    w.show();
    return a.exec();
}
