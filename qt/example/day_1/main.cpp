﻿#include "mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QDialog>
#include <QString>
#include <QStringData>
#include <qlabel.h>

QString pointToString(const QPoint& p){
    return QString("%1,%2").arg(p.x()).arg(p.y());
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QDialog dialog;
    dialog.resize(400,300);
    QLabel label(&dialog);
    label.setText("hello world qt");
    dialog.show();

    QPoint p(0,0);
    QString qs;
    QDataStream data;
    data<<p;
    data>>qs;
    qDebug()<<qs;
    qDebug()<<p;
    qDebug()<<pointToString(p);
    return a.exec();
}
